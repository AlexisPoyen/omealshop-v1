<?php
include_once "../view/nav.php";

?>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <strong>Inscription</strong> Pour pouvoir utiliser le site, veuillez remplir ce formulaire, puis activer votre compte par le mail qui vous sera envoyé ensuite.
                </div>
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="alert alert-danger nom" style="display:none;">
                    <strong>Erreur !</strong> Le nom que vous aviez saisi n'est pas correct.
                </div>

                <div class="alert alert-danger prenom" style="display:none;">
                    <strong>Erreur !</strong> Le prénom que vous aviez saisi n'est pas correct.
                </div>

                <div class="alert alert-danger mailFormat" style="display:none;">
                    <strong>Erreur !</strong> Le format du mail que vous aviez saisi n'est pas correct.
                </div>

                <div class="alert alert-danger mailExiste" style="display:none;">
                    <strong>Erreur !</strong> Le mail que vous aviez saisi est déjà associé à un compte.
                </div>

                <div class="alert alert-danger mdpIdentique" style="display:none;">
                    <strong>Erreur !</strong> Les mot de passes que vous aviez saisis ne correspondent pas.
                </div>

                <form id="inscription" role="form">

                    <div class="form-group">
                        <label>Nom</label>
                        <input class="form-control" name="nom" placeholder="Nom">
                    </div>

                    <div class="form-group">
                        <label>Prénom</label>
                        <input class="form-control" name="prenom" placeholder="Prénom">
                    </div>

                    <div class="form-group">
                        <label>Mail</label>
                        <input type="email" name="mail" class="form-control" placeholder="Mail">
                        <p class="help-block">Le mail ne doit pas déjà être utiliser pour un autre compte</p>
                    </div>

                    <div class="form-group">
                        <label>Mot de passe</label>
                        <input type="password" name="mdp" class="form-control">
                        <p class="help-block">Recommendation pour la sécurité du mot de passe : doit comporter une majuscule, un chiffre et un caractère spécial (tous sauf lettre et chiffre) et être d'une taille de 8 caractères minimum</p>
                    </div>

                    <div class="form-group">
                        <label>Confirmer mot de passe</label>
                        <input type="password" name="mdpConfirm" class="form-control">
                        <p class="help-block">Doit être identique au précédent.</p>
                    </div>

                    <button id="btnInscription" type="button" class="btn btn-default">Valider</button>
                    <button type="reset" class="btn btn-default">Réinitialiser les champs</button>

                </form>

            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <script src="../js/inscription.js"></script>
    <script src="../js/main.js"></script>

<?php
require_once '../view/footer.php';
