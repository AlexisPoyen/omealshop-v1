<?php
require_once "../view/nav.php";
?>
<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
        <li class="active" style="background-color: #1d1d1d !important;">
            <a href="home.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
        </li>
        <li>
            <a href="planning.php"><i class="fa fa-fw fa-table"></i> Plannifier des repas</a>
        </li>
        <li>
            <a href="produits.php"><i class="fa fa-fw fa-edit"></i> Ajouter un produit</a>
        </li>
        <li>
            <a href="gestionRepas.php"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
        </li>
        <li>
            <a href="gestionListe.php"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
        </li>
    </ul>
</div>
<!-- /.navbar-collapse -->
</nav>

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <strong>OMealShop :</strong> Organiser votre liste de course en fonction de vos repas
                </h1>
            </div>
        </div>

        <div id="principeOMealShop" class="row" >
            <div class="col-lg-12">
                <h2 class="page-header">Principes de OMealShop</h2>
                <p>OMealShop est un site réalisé par un amateur dans le but d'aider les personnes à gérer leur liste de courses plus facilement en la partageant avec leur foyer directement en ligne. <br/>
                Ce site est totalement amateur, il n'est affilié à aucune entreprise ou association et ne génére aucun revenu pour son créateur. Vos informations ne seront jamais communiqué à aucun organisme que ce soit.</p>
            </div>
            <div class="col-lg-12">
                <h2 class="page-header">Vos droits</h2>
                <p>OMealShop étant un site amateur, il est dispensé de déclaration auprès de la CNIL par la <a href="https://www.cnil.fr/fr/dispense/di-006-sites-web-personnels-ou-blogs">"Dispense DI-006"</a>
                <blockquote>

                    La dispense n°6 concerne les sites web ou blogs mis en œuvre par des particuliers à titre privé qui peuvent permettre, d’une part, la collecte de données à caractère personnel de personnes qui s’y connectent et, d’autre part, la diffusion de données à caractère personnel (nom, images de personnes ou tout autre élément permettant d’identifier une personne physique).

                    La diffusion et la collecte de données à caractère personnel opérées à partir d’un site web dans le cadre d’activités professionnelles, politiques, ou associatives restent soumises à une déclaration préalable auprès de la CNIL.

                    <h4>Texte officiel</h4>

                    Délibération n°2005-284 du 22/11/2005 décidant la dispense de déclaration des sites web diffusant ou collectant des données à caractère personnel mis en oeuvre par des particuliers dans le cadre d'une activité exclusivement personnelle.
                </blockquote>
                Vous conservez cependant vos droits de consulter, d'éditer ou de supprimer vos informations personelles en contactant le webmaster <a href="mailto:poyen.alexis.info@gmail.com">Alexis POYEN</a>.
                </p>
            </div>
        </div>
        <!-- div row -->

    </div>
    <!-- /.container-fluid -->
    <script src="../js/index.js"></script>
    <script src="../js/main.js"></script>

<?php
require_once '../view/footer.php';
    
