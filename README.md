![alt](image/logo-black.png)

# OMealShop

[OMealShop](http://omealshop.alwaysdata.net/)

## Description

OmealShop est un site vous permettant de générer facilement vos listes de courses pour tout votre foyer.

Vous pouvez :

- Plannifier vos repas
  ![alt](image/plan_meal.png)
- Ajouter des produits à vos listes de courses
  ![alt](image/add_products.png)
- Ajouter plusieurs membres à votre foyer
- Créer des repas ou modifier ceux existants
  ![alt](image/modify_meal.png)

## Déploiement

Pour déployer l'application cloner le dépôt git sur un serveur web PHP.

Créer une base de donnée de type SQL et y executer le script create_db.sql.

Renommer le fichier config.inc.default.php en config.inc.php et y apporter les modifications pour correspondre à votre environnement, nottament la connexion à la base de donnée.

## Environnement
Application développée et déployée avec :
- PHP 7.0
- JavaScript / JQuery
- HTML5 / CSS3
- Bootstrap / Theme "SB Admin" de startbootstrap.com
- MySQL 5.6

## RoadMap

L'application sera prochainement redéveloppée avec de nouvelles technos :

- NodeJS en backend
- Angular ou View en frontend
- API en GraphQL ou Rest
- PostGreSQL
- Docker

Liste des choses à faire :

- Proposer un moyen simple de traduire l'application
- Corriger les quelques bugs

## Licence

OMealShop est sous licence GNU GPLv3