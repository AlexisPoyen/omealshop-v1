<?php
require_once 'personne.php';
require_once 'foyer.php';
require_once 'produit.php';
require_once 'ingredients.php';

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:11
 */
class ingredients_listes
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var bool|ingredients
     */
    private $ingredients;
    /**
     * @var bool|foyer
     */
    private $foyer;
    /**
     * @var bool|personne
     */
    private $demandeur;
    /**
     * @var int
     */
    private $quantite;
    /**
     * @var string
     */
    private $commentaire;

    /**
     * produits_listes constructor.
     * @param $id
     * @param $ingredients
     * @param $foyer
     * @param $demandeur
     * @param $quantite
     */
    public function __construct($id, $ingredients, $foyer, $demandeur, $quantite, $commentaire)
    {
        $this->id = $id;
        $this->ingredients = ingredients::getById($ingredients);
        $this->foyer = foyer::getById($foyer);
        $this->demandeur = personne::getById($demandeur);
        $this->quantite = $quantite;
        $this->commentaire = $commentaire;
    }

    /**
     * @param $id
     * @return bool|ingredients_listes
     */
    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM ingredients_listes WHERE INGREDIENTS_LISTES_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new ingredients_listes($res['INGREDIENTS_LISTES_id'],
                $res['INGREDIENTS_LISTES_ingredients_id'],
                $res['INGREDIENTS_LISTES_foyer_id'],
                $res['INGREDIENTS_LISTES_demandeur'],
                $res['INGREDIENTS_LISTES_quantite'],
                $res['INGREDIENTS_LISTES_commentaire']);
        else
            return false;
    }

    /**
     * @param $ingredient
     * @param $foyer
     * @param $personne
     * @param $quantite
     * @param $commentaire
     * @return bool
     */
    public static function addProduitToListe($ingredient, $foyer, $personne, $quantite, $commentaire)
    {
        $reqInsertIngredient = PDO_OMealShop::connexionBDD()->prepare("INSERT INTO ingredients_listes(INGREDIENTS_LISTES_ingredients_id,
                    INGREDIENTS_LISTES_foyer_id,
                    INGREDIENTS_LISTES_demandeur,
                    INGREDIENTS_LISTES_quantite,
                    INGREDIENTS_LISTES_commentaire) 
                    VALUES(:idIngredient, :idFoyer, :idPersonne, :quantite, :commentaire);");
        $reqInsertIngredient->execute(array(':idIngredient' => $ingredient->getId(), ':idFoyer' => $foyer->getId(), ':idPersonne' => $personne->getId(), ':quantite' => $quantite, ':commentaire' => $commentaire));
        if($reqInsertIngredient->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @param $foyer
     * @return array
     */
    public static function getByFoyer($foyer)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM ingredients_listes WHERE INGREDIENTS_LISTES_foyer_id = :id');
        $reqGetById->execute(array(':id' => $foyer->getId()));
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $res) {
            $liste[] = ingredients_listes::getById($res['INGREDIENTS_LISTES_id']);
        }
        return $liste;
    }

    /**
     * @param $idIngredient
     * @return bool
     */
    public static function supprByIdIngredient($idIngredient)
    {
        $reqDeleteProduit = PDO_OMealShop::connexionBDD()->prepare("DELETE FROM ingredients_listes WHERE INGREDIENTS_LISTES_ingredients_id = :id;");
        $reqDeleteProduit->execute(array(':id' => $idIngredient));
        if($reqDeleteProduit->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @return bool
     */
    public function suppr()
    {
        $reqDeleteProduit = PDO_OMealShop::connexionBDD()->prepare("DELETE FROM ingredients_listes WHERE INGREDIENTS_LISTES_id = :id;");
        $reqDeleteProduit->execute(array(':id' => $this->id));
        if($reqDeleteProduit->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @return mixed
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool|personne
     */
    public function getDemandeur()
    {
        return $this->demandeur;
    }

    /**
     * @return bool|ingredients
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }
}