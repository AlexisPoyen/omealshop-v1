<?php

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:06
 */
class categorie_repas
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $intitule;

    /**
     * categorie_repas constructor.
     * @param $id int
     * @param $intitule string
     */
    public function __construct($id, $intitule)
    {
        $this->id = $id;
        $this->intitule = $intitule;
    }

    /**
     * @param $id
     * @return bool|categorie_repas
     */
    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM categorie_repas WHERE CATEGORIE_REPAS_id= :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new categorie_repas($res['CATEGORIE_REPAS_id'],
                $res['CATEGORIE_REPAS_intitule']);
        else
            return false;
    }

    /**
     * @return array<categorie_repas>
     */
    public static function getAll(){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM categorie_repas');
        $reqGetById->execute();
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $resultat) {
            $categories[] = self::getById($resultat['CATEGORIE_REPAS_id']);
        }
        return $categories;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }
}