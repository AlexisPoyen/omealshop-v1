<?php
require_once 'categorie_produits.php';

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:08
 */
class produit
{
    private $id;
    private $categorie_produit;
    private $nom;
    private $unite;

    /**
     * produit constructor.
     * @param $id
     * @param $categorie_produit
     * @param $nom
     * @param $unite
     */
    public function __construct($id, $categorie_produit, $nom, $unite)
    {
        $this->id = $id;
        $this->categorie_produit = categorie_produits::getById($categorie_produit);
        $this->nom = $nom;
        $this->unite = $unite;
    }

    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM produit WHERE PRODUIT_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new produit($res['PRODUIT_id'],
                $res['PRODUIT_categorie_produit_id'],
                $res['PRODUIT_nom'],
                $res['PRODUIT_unite']);
        else
            return false;
    }

    public static function getListeByCat($cat)
    {
        $reqGetAll = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM produit where PRODUIT_categorie_produit_id = :idCat ORDER BY PRODUIT_nom;');
        $reqGetAll->execute(array(':idCat' => $cat));
        $resultats = $reqGetAll->fetchAll();
        foreach ($resultats as $res) {
            $produits[] = new produit($res['PRODUIT_id'],
                $res['PRODUIT_categorie_produit_id'],
                $res['PRODUIT_nom'],
                $res['PRODUIT_unite']);
        }
        return $produits;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return bool|categorie_produits
     */
    public function getCategorieProduit()
    {
        return $this->categorie_produit;
    }

    /**
     * @return mixed
     */
    public function getUnite()
    {
        return $this->unite;
    }
}