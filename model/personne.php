<?php

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 19:51
 */
require_once 'DB.php';
require_once '../config.inc.php';

class personne
{
    private $id;
    private $nom;
    private $prenom;
    private $mail;
    private $mdp;
    private $etat;
    private $create_time;

    /**
     * personne constructor.
     * @param $id int
     * @param $nom string
     * @param $prenom string
     * @param $mail string
     * @param $mdp string
     * @param $etat bool
     * @param $create_time DateTime
     */
    public function __construct($id, $nom, $prenom, $mail, $mdp, $etat, $create_time)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mail = $mail;
        $this->mdp = $mdp;
        $this->etat = $etat;
        $this->create_time = new DateTime($create_time);
    }

    public static function getById($id)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM personne WHERE PERSONNE_id= :id');
        $reqGetById->execute(array(':id' => $id));
        if ($res = $reqGetById->fetch())
            return new personne($res['PERSONNE_id'],
                $res['PERSONNE_nom'],
                $res['PERSONNE_prenom'],
                $res['PERSONNE_mail'],
                $res['PERSONNE_mdp'],
                $res['PERSONNE_etat'],
                $res['PERSONNE_create_time']);
        else
            return false;
    }

    public static function getByMail($mail)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM personne WHERE PERSONNE_mail= :mail');
        $reqGetById->execute(array(':mail' => $mail));
        if ($res = $reqGetById->fetch())
            return new personne($res['PERSONNE_id'],
                $res['PERSONNE_nom'],
                $res['PERSONNE_prenom'],
                $res['PERSONNE_mail'],
                $res['PERSONNE_mdp'],
                $res['PERSONNE_etat'],
                $res['PERSONNE_create_time']);
        else
            return false;
    }

    public static function inscription($nom, $prenom, $mail, $mdp)
    {
        $reqInsert = PDO_OMealShop::connexionBDD()->prepare('INSERT INTO personne(PERSONNE_nom, PERSONNE_prenom, PERSONNE_mail, PERSONNE_mdp, PERSONNE_etat, PERSONNE_create_time) 
                                                              VALUES(:nom, :prenom, :mail, :mdp, 0, NOW())');
        $reqInsert->execute(array(':nom' => $nom, ':prenom' => $prenom, ':mail' => $mail, ':mdp' => hash('sha256', $mdp)));
        $personne = personne::getByMail($mail);
        if ($reqInsert->rowCount() > 0) {
            $destinataire = $mail;
            $sujet = "Nouvelle inscription";
            $entete = "From: no_reply@omealshop.com \n";
            $message = 'Bonjour,

Vous venez de vous inscrire sur le site OMealShop, pour valider votre profil, cliquez sur le lien suivant : 

' . TXT_addresse_site . '/ctrl/confirmSignUp.php?id=' . $personne->getId() . '
            
---------------
Ceci est un mail automatique, Merci de ne pas y répondre.';

            return mail($destinataire, $sujet, $message, $entete); // Envoi du mail
        }
        return false;
    }

    public static function getPersonnesLikeNotMembre($search, $idFoyer)
    {
        $reqGetBySearch = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM personne WHERE (PERSONNE_mail LIKE :search OR PERSONNE_nom LIKE :search OR PERSONNE_prenom LIKE :search )
                                                                  AND PERSONNE_id NOT IN (SELECT MEMBRE_FOYER_personne_id FROM membre_foyer WHERE MEMBRE_FOYER_foyer_id = :idFoyer);');
        $reqGetBySearch->execute(array(':search' => '%' . $search . '%', ':idFoyer' => $idFoyer));
        $resultats = $reqGetBySearch->fetchAll();
        foreach ($resultats as $res) {
            $personnes[] = new personne($res['PERSONNE_id'],
                $res['PERSONNE_nom'],
                $res['PERSONNE_prenom'],
                $res['PERSONNE_mail'],
                $res['PERSONNE_mdp'],
                $res['PERSONNE_etat'],
                $res['PERSONNE_create_time']);
        }
        return $personnes;
    }

    public function activerCompte()
    {
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE personne SET PERSONNE_etat = 1 WHERE PERSONNE_id = :id');
        $reqUpdate->execute(array(':id' => $this->id));
        if ($reqUpdate->rowCount() > 0)
            return true;
        return false;
    }

    public function connecter($mdpSaisit)
    {
        if ($this->etat == 1) {
            if ($this->mdp == hash('sha256', $mdpSaisit)) {
                $session = new session($this);
                $_SESSION['session'] = serialize($session);
                return 0;
            } else
                return -2;
        } else
            return -1;
    }

    public function changerMotDePasse($mdp)
    {
        if($this->mdp == hash('sha256', $mdp))
            return true;
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE personne SET PERSONNE_mdp = :mdp WHERE PERSONNE_id = :id');
        $reqUpdate->execute(array(':id' => $this->id, ':mdp' => hash('sha256', $mdp)));
        $this->mdp = hash('sha256', $mdp);
        if ($reqUpdate->rowCount() > 0)
            return true;
        return false;
    }

    public function suppr()
    {
        $foyers = membre_foyer::getFoyersPersonne($this->id);
        if (count($foyers))
            foreach ($foyers as $foyer) {
                membre_foyer::supprMembre($foyer->getId(), $this->id);
            }
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE personne SET PERSONNE_etat = 2 WHERE PERSONNE_id = :id');
        $reqUpdate->execute(array(':id' => $this->id));
        if ($reqUpdate->rowCount() > 0)
            return true;
        return false;

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        if ($this->mail == $mail)
            return true;
        else {
            $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE personne SET PERSONNE_mail = :mail WHERE PERSONNE_id = :id');
            $reqUpdate->execute(array(':id' => $this->id, ':mail' => $mail));
            $this->mail = $mail;
            if ($reqUpdate->rowCount() > 0)
                return true;
            return false;
        }
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        if ($this->nom == $nom)
            return true;
        else {
            $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE personne SET PERSONNE_nom = :nom WHERE PERSONNE_id = :id');
            $reqUpdate->execute(array(':id' => $this->id, ':nom' => $nom));
            $this->nom = $nom;
            if ($reqUpdate->rowCount() > 0)
                return true;
            return false;
        }
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        if ($this->prenom == $prenom)
            return true;
        else {
            $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE personne SET PERSONNE_prenom = :prenom WHERE PERSONNE_id = :id');
            $reqUpdate->execute(array(':id' => $this->id, ':prenom' => $prenom));
            $this->prenom = $prenom;
            if ($reqUpdate->rowCount() > 0)
                return true;
            return false;
        }
    }
}