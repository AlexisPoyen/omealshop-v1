<?php

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:04
 */
class categorie_ingredients
{
    private $id;
    private $intitule;

    /**
     * categorie_ingredients constructor.
     * @param $id int
     * @param $intitule string
     */
    public function __construct($id, $intitule)
    {
        $this->id = $id;
        $this->intitule = $intitule;
    }

    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM categorie_ingredients WHERE CATEGORIE_INGREDIENTS_id= :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new categorie_ingredients($res['CATEGORIE_INGREDIENTS_id'],
                $res['CATEGORIE_INGREDIENTS_intitule']);
        else
            return false;
    }

    public static function getListe()
    {
        $reqGetAll = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM categorie_ingredients;');
        $reqGetAll->execute(array());
        $resultats = $reqGetAll->fetchAll();
        foreach ($resultats as $res) {
            $categories[] = new categorie_ingredients($res['CATEGORIE_INGREDIENTS_id'],
                $res['CATEGORIE_INGREDIENTS_intitule']);
        }
        return $categories;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}