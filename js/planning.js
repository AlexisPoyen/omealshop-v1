var tab = [];
var moiCourant;
var anneeCourante;
var joursSemaine = new Array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
var mois = new Array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
var joursDansMoi = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

function afficherTableau() {
    var tableau = [];
    var dateActuelle = new Date();
    dateActuelle.setDate(dateActuelle.getDate() - 1);
    var jourCourant = 1;
    var total = joursDansMoi[moiCourant];
    var dateDebutMoi = new Date(anneeCourante, moiCourant, 1);
    var t = $('<table class="table table-bordered table-striped" />');
    var tbody = $('<tbody />');
    var thead = $('<thead />');
    var td;
    var tempsSuppr = null;
    $.ajax({
        type: $(this).attr('method'),
        url: '../ctrl/gestionFoyer.php?action=getTempsSuppr'
    }).done(function (data) {
        if (data.ok) {
            tempsSuppr = data.tempsSuppr;
            loop_i:
                for (var i = 0; i < 7; i++) {
                    tableau[i] = [];
                    if (i == 0) {
                        var tr = $('<tr />');
                        for (var y = 0; y < 7; y++) {
                            tableau[i][y] = joursSemaine[y];
                            var th = $('<th />');
                            th.html(joursSemaine[y]);
                            th.appendTo(tr)
                        }
                        tr.appendTo(thead);
                    }
                    else {
                        tr = $('<tr />');
                        for (var j = 0; j < 7; j++) {
                            if (i == 1) {
                                if (j < dateDebutMoi.getDay()) {
                                    tableau[i][j] = null;
                                    td = $('<td />');
                                    td.appendTo(tr);
                                }
                                else {
                                    tableau[i][j] = i + j;
                                    td = addTdInCalendar(anneeCourante, moiCourant, jourCourant, dateActuelle, tempsSuppr);
                                    td.appendTo(tr);
                                    jourCourant++;
                                }
                            }
                            else if (i == 5 || i == 6) {
                                if (jourCourant > total) {
                                    tableau[i][j] = null;
                                    td = $('<td />');
                                    td.appendTo(tr);
                                }
                                else {
                                    tableau[i][j] = i + j;
                                    td = addTdInCalendar(anneeCourante, moiCourant, jourCourant, dateActuelle, tempsSuppr);
                                    td.appendTo(tr);
                                    jourCourant++;
                                }
                            }
                            else {
                                tableau[i][j] = i + j;
                                td = addTdInCalendar(anneeCourante, moiCourant, jourCourant, dateActuelle, tempsSuppr);
                                td.appendTo(tr);
                                jourCourant++;
                            }
                        }
                        tr.appendTo(tbody);
                        if (i == 5 && jourCourant > total)
                            break loop_i;
                    }
                }
            thead.appendTo(t);
            tbody.appendTo(t);
            t.appendTo('#calendrier');
            tab = tableau;

            $('.future').click(function () {
                var date = new Date($(this).data('annee'), $(this).data('moi'), $(this).data('jour'));
                var dateSql = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                var jourSelect = joursSemaine[date.getDay()] + ' ' + date.getDate().toString() + ' ' + mois[date.getMonth()] + ' ' + date.getFullYear().toString();
                $(".jourSelect").html(jourSelect);
                $('.date').val(dateSql);
                $('#plannifieur').slideDown();
                chargerRepasJour(dateSql);
                $('.future').css('background-color', '#286090').css('color', 'white');
                $('.passee').css('background-color', '#FF6600');
                $(this).css("backgroundColor", '#009700');
            });

            $('.passee').click(function () {
                var date = new Date($(this).data('annee'), $(this).data('moi'), $(this).data('jour'));
                var dateSql = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                var jourSelect = joursSemaine[date.getDay()] + ' ' + date.getDate().toString() + ' ' + mois[date.getMonth()] + ' ' + date.getFullYear().toString();
                $(".jourSelect").html(jourSelect);
                $('.date').val(dateSql);
                $('#plannifieur').slideUp();
                $('.passee').css('background-color', '#FF6600');
                $('.future').css('background-color', '#286090').css('color', 'white');
                $(this).css("backgroundColor", '#009700');

                chargerRepasJour(dateSql);
            });
        }
    }).fail(erreurCritique);
}

function addTdInCalendar(anneeCourante, moiCourant, jourCourant, dateActuelle, tempsSuppr) {
    td = $('<td />');
    td.html(jourCourant);
    td.data('jour', jourCourant);
    td.data('moi', moiCourant);
    td.data('annee', anneeCourante);
    if (dateActuelle < new Date(anneeCourante, moiCourant, jourCourant)) {
        td.addClass('active future');
        td.css('background-color', '#286090').css('color', 'white');
    }
    else {
        var datePasseeActive = new Date();
        datePasseeActive.setDate(datePasseeActive.getDate() - (tempsSuppr * 7));
        if (datePasseeActive < new Date(anneeCourante, moiCourant, jourCourant)) {
            td.addClass('active passee');
            td.css('background-color', '#FF6600');
        }
    }
    return td;
}

function chargerCalendrier() {
    var date = new Date();
    moiCourant = date.getMonth();
    anneeCourante = date.getFullYear();
    if (anneeCourante % 4 == 0)
        joursDansMoi[1] = 29;
    else
        joursDansMoi[1] = 28;
    $("#moisCourant").html(mois[moiCourant]);
    $("#anneeCourante").html(anneeCourante);
    afficherTableau();
}

function chargerMoiPrecedent() {
    $('table').remove();
    if (moiCourant == 0)
        moiCourant = 12;
    if (moiCourant == 12)
        anneeCourante--;
    moiCourant--;
    if (anneeCourante % 4 == 0)
        joursDansMoi[1] = 29;
    else
        joursDansMoi[1] = 28;
    $("#moisCourant").html(mois[moiCourant]);
    $("#anneeCourante").html(anneeCourante);

    afficherTableau();
}

function chargerMoiSuivant() {
    $('table').remove();
    if (moiCourant == 11)
        moiCourant = -1;
    if (moiCourant == -1) {
        anneeCourante++;
    }
    moiCourant++;
    if (anneeCourante % 4 == 0)
        joursDansMoi[1] = 29;
    else {
        joursDansMoi[1] = 28;
    }
    $("#moisCourant").html(mois[moiCourant]);
    $("#anneeCourante").html(anneeCourante);

    afficherTableau();
}

function chargerNbPersonnes() {
    var select = $('.nbPersonne');
    var option = $('<option />');
    for (var i = 1; i < 25; i++) {
        option = new Option(i + " personne(s)", i);
        select.append(option);
    }
}

function chargerRepas(repas, cat) {
    $.ajax({
        type: 'get',
        url: '../ctrl/getRepas.php?type='+repas +'&cat='+cat
    }).done(function (data) {
        var select = $('.' + repas).empty();
        select.append('<option value="aucun">aucun</option>');
        var option = $('<option />');
        for (var i in data.repas) {
            var retour = jQuery.parseJSON(data.repas[i]);
            option = new Option(retour.nomRepas, retour.idRepas);
            select.append(option);
        }
    }).fail(erreurCritique);
}

function chargerRepasJour(dateSql) {
    $('#planPetitDejeuner').html('aucun');
    $('#planEntreeDejeuner').html('aucun');
    $('#planPlatDejeuner').html('aucun');
    $('#planDessertDejeuner').html('aucun');
    $('#planEntreeDiner').html('aucun');
    $('#planPlatDiner').html('aucun');
    $('#planDessertDiner').html('aucun');
    $.ajax({
        type: 'get',
        url: '../ctrl/getRepas.php?date=' + dateSql
    }).done(function (data) {
        for (var i in data.repasPlanifie) {
            var retour = jQuery.parseJSON(data.repasPlanifie[i]);
            var dateRepas = new Date(retour.dateRepas);
            if (dateRepas.getTime() == new Date(dateSql + " 8:0:0").getTime()) {
                $('#planPetitDejeuner').html(retour.nomRepas + " pour " + retour.nbPersonne + " personne(s)   " + ' par '+retour.prenomDemandeur+' '+retour.nomDemandeur+'<button type="button" class="supprimer btn btn-sm btn-danger" value="' + retour.dateRepas + '">Supprimer</button>');
            }
            if (dateRepas.getTime() == new Date(dateSql + " 12:0:0").getTime()) {
                $('#planEntreeDejeuner').html(retour.nomRepas + " pour " + retour.nbPersonne + " personne(s)   " +  ' par '+retour.prenomDemandeur+' '+retour.nomDemandeur+'<button type="button" class="supprimer btn btn-sm btn-danger" value="' + retour.dateRepas + '">Supprimer</button>');
            }
            if (dateRepas.getTime() == new Date(dateSql + " 12:30:0").getTime()) {
                $('#planPlatDejeuner').html(retour.nomRepas + " pour " + retour.nbPersonne + " personne(s)   " + ' par '+retour.prenomDemandeur+' '+retour.nomDemandeur+ '<button type="button" class="supprimer btn btn-sm btn-danger" value="' + retour.dateRepas + '">Supprimer</button>');
            }
            if (dateRepas.getTime() == new Date(dateSql + " 13:0:0").getTime()) {
                $('#planDessertDejeuner').html(retour.nomRepas + " pour " + retour.nbPersonne + " personne(s)   " + ' par '+retour.prenomDemandeur+' '+retour.nomDemandeur+ '<button type="button" class="supprimer btn btn-sm btn-danger" value="' + retour.dateRepas + '">Supprimer</button>');
            }
            if (dateRepas.getTime() == new Date(dateSql + " 19:0:0").getTime()) {
                $('#planEntreeDiner').html(retour.nomRepas + " pour " + retour.nbPersonne + " personne(s)   " + ' par '+retour.prenomDemandeur+' '+retour.nomDemandeur+ '<button type="button" class="supprimer btn btn-sm btn-danger" value="' + retour.dateRepas + '">Supprimer</button>');
            }
            if (dateRepas.getTime() == new Date(dateSql + " 19:30:0").getTime()) {
                $('#planPlatDiner').html(retour.nomRepas + " pour " + retour.nbPersonne + " personne(s)   " + ' par '+retour.prenomDemandeur+' '+retour.nomDemandeur+ '<button type="button" class="supprimer btn btn-sm btn-danger" value="' + retour.dateRepas + '">Supprimer</button>');
            }
            if (dateRepas.getTime() == new Date(dateSql + " 20:0:0").getTime()) {
                $('#planDessertDiner').html(retour.nomRepas + " pour " + retour.nbPersonne + " personne(s)   " + ' par '+retour.prenomDemandeur+' '+retour.nomDemandeur+ '<button type="button" class="supprimer btn btn-sm btn-danger" value="' + retour.dateRepas + '">Supprimer</button>');
            }
        }

        $('.supprimer').click(function () {
            $.ajax({
                type: 'get',
                url: '../ctrl/planningRepas.php?moment=' + 'supprimer' + '&date=' + $(this).val()
            }).done(function (data) {
                if (data.ok) {
                    $('#succesSuppr').show();
                    $('#erreurSuppr').hide();
                }
                else {
                    $('#succesSuppr').hide();
                    $('#erreurSuppr').show();
                }
                chargerRepasJour(dateSql);
            }).fail(erreurCritique);

        });
    }).fail(erreurCritique);
}

function chargerRepasSansDate(){
    $.ajax({
        type: 'get',
        url: '../ctrl/getRepas.php?date=2000-01-01'
    }).done(function (data) {
        var tbody = $('#tbl-repas-sans-date');
        for (var i in data.repasPlanifie) {
            var retour = jQuery.parseJSON(data.repasPlanifie[i]);
            var tr = '<tr id="plat'+retour.idPlan+'"><td>'+retour.nomRepas+'</td><td>'+retour.nbPersonne+'</td><td>'+retour.prenomDemandeur+' '+retour.nomDemandeur+'</td><td><button type="button" class="supprimerNoDate btn btn-sm btn-danger" value="' + retour.idPlan + '">Supprimer</button></td></tr>';
            tbody.append(tr);
        }

        $('.supprimerNoDate').click(function () {
            $.ajax({
                type: 'get',
                url: '../ctrl/planningRepas.php?moment=' + 'supprimer' + '&id=' + $(this).val()
            }).done(function (data) {
                if (data.ok) {
                    $('#succesSupprSansDate').show();
                    $('#erreurSupprSansDate').hide();
                    $('#plat'+data.idPlan).hide();
                }
                else {
                    $('#succesSupprSansDate').hide();
                    $('#erreurSupprSansDate').show();
                }
            }).fail(erreurCritique);

        });
    }).fail(erreurCritique);

}

$(document).ready(function () {
    $.ajax({
        type: 'get',
        url: '../ctrl/estConnecte.php'
    }).done(function (data) {
        if (data) {
            $("#connecte").show();
            $("#navigationBarre").show();
            $("#racourcisIndex").show();
        }
        else {
            document.location.href = "http://omealshop.alwaysdata.net/index.php"
        }
    }).fail(erreurCritique);

    chargerCalendrier();
    $('#moiPrecedent').click(chargerMoiPrecedent);
    $('#moiSuivant').click(chargerMoiSuivant);

    chargerRepas("petit-dejeuner", 'tout');
    chargerRepas("entree", 'tout');
    chargerRepas("plat", 'tout');
    chargerRepas("dessert", 'tout');
    chargerRepasSansDate();
    chargerNbPersonnes()

    $('#lst-categorie-repas').change(function () {
        chargerRepas("petit-dejeuner", this.value);
        chargerRepas("entree", this.value);
        chargerRepas("plat", this.value);
        chargerRepas("dessert", this.value);
    })


    $('#boutonPetitDejeuner').click(function () {
        $('#petitDejeuner').show();
        $('#dejeuner').hide();
        $('#diner').hide();
    });

    $('#boutonDejeuner').click(function () {
        $('#petitDejeuner').hide();
        $('#dejeuner').show();
        $('#diner').hide();
    });

    $('#boutonDiner').click(function () {
        $('#petitDejeuner').hide();
        $('#dejeuner').hide();
        $('#diner').show();

    });

    $('#enrouler').click(function () {
        $('#petitDejeuner').hide();
        $('#dejeuner').hide();
        $('#diner').hide();
    });

    $('#succes').hide();
    $('#erreur').hide();

    $("#petitDejeuner").submit(function () {
        var date = $( "#no-date:checked" ).length;
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action')+'&date='+date,
            data: $(this).serialize()
        }).done(function (data) {
            if (data.ok) {
                $('#succes').show();
                $('#erreur').hide();
                if(date == 1)
                    chargerRepasSansDate();
            }
            else {
                $('#erreur ul').empty();
                var ul = $('<ul />');
                for (var j in data.erreur) {
                    ul.append($('<li>').html(data.erreur[j]));
                }
                $('#succes').hide();

                $('#erreur').append(ul).show();
                for (var i in data.repasPlannifie) {
                    var retour = jQuery.parseJSON(data.repasPlannifie[i]);
                    var question = 'Vous aviez déjà plannifié un(e) ' + retour.typeRepas + ' : ' + retour.ancienRepas
                        + ' pour ' + retour.nbPersonne + ' personne(s). Voulez vous le remplacer par ' + retour.nouveauRepas
                        + ' pour ' + retour.newNbPersonne + ' personne(s) ?';
                    var r = confirm(question);
                    if (r) {
                        $.ajax({
                            type: 'get',
                            url: '../ctrl/planningRepas.php?moment=' + 'ajout' + '&id=' + retour.idNouveauRepas + '&date=' + retour.date + '&nb=' + retour.newNbPersonne
                        }).done(function (data) {
                            if (data.ok) {
                                $('#succes').show();
                                $('#erreur').hide();
                            }
                            else {
                                $('#erreur ul').empty();
                                var ul = $('<ul />');
                                var li = $('<li />');
                                for (var j in data.erreur) {
                                    ul.append($('<li>').html(data.erreur[j]));
                                }
                                $('#succes').hide();

                                $('#erreur').append(ul).show();
                            }
                        }).fail(erreurCritique);
                        chargerRepasJour($('.date').val());
                        chargerRepasSansDate();
                    }
                }
            }
        }).fail(erreurCritique);
        chargerRepasJour($('.date').val());
        return false;  //a garder sinon va recharger la page
    });

    $("#dejeuner").submit(function () {
        var date = $( "#no-date:checked" ).length;
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action')+'&date='+date,
            data: $(this).serialize()
        }).done(function (data) {
            if (data.ok) {
                $('#succes').show();
                $('#erreur').hide();
                if(date == 1)
                    chargerRepasSansDate();
            }
            else {
                $('#erreur ul').empty();
                var ul = $('<ul />');
                var li = $('<li />');
                for (var j in data.erreur) {
                    ul.append($('<li>').html(data.erreur[j]));
                }
                $('#succes').hide();

                $('#erreur').append(ul).show();
                for (var i in data.repasPlannifie) {
                    var retour = jQuery.parseJSON(data.repasPlannifie[i]);
                    var question = 'Vous aviez déjà plannifié un(e) ' + retour.typeRepas + ' : ' + retour.ancienRepas
                        + ' pour ' + retour.nbPersonne + ' personne(s). Voulez vous le remplacer par ' + retour.nouveauRepas
                        + ' pour ' + retour.newNbPersonne + ' personne(s) ?';
                    var r = confirm(question);
                    if (r) {
                        $.ajax({
                            type: 'get',
                            url: '../ctrl/planningRepas.php?moment=' + 'ajout' + '&id=' + retour.idNouveauRepas + '&date=' + retour.date + '&nb=' + retour.newNbPersonne
                        }).done(function (data) {
                            if (data.ok) {
                                $('#succes').show();
                                $('#erreur').hide();
                            }
                            else {
                                $('#erreur ul').empty();
                                var ul = $('<ul />');
                                var li = $('<li />');
                                for (var j in data.erreur) {
                                    ul.append($('<li>').html(data.erreur[j]));
                                }
                                $('#succes').hide();

                                $('#erreur').append(ul).show();
                            }
                        }).fail(erreurCritique);
                        chargerRepasJour($('.date').val());
                        chargerRepasSansDate();
                    }

                }
            }
        }).fail(erreurCritique);
        chargerRepasJour($('.date').val());
        return false;  //a garder sinon va recharger la page
    });

    $("#diner").submit(function () {
        var date = $( "#no-date:checked" ).length;
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action')+'&date='+date,
            data: $(this).serialize()
        }).done(function (data) {
            if (data.ok) {
                $('#succes').show();
                $('#erreur').hide();
                if(date == 1)
                    chargerRepasSansDate();
            }
            else {
                $('#erreur ul').empty();
                var ul = $('<ul />');
                var li = $('<li />');
                for (var j in data.erreur) {
                    ul.append($('<li>').html(data.erreur[j]));
                }
                $('#succes').hide();

                $('#erreur').append(ul).show();
                for (var i in data.repasPlannifie) {
                    var retour = jQuery.parseJSON(data.repasPlannifie[i]);
                    var question = 'Vous aviez déjà plannifié un(e) ' + retour.typeRepas + ' : ' + retour.ancienRepas
                        + ' pour ' + retour.nbPersonne + ' personne(s). Voulez vous le remplacer par ' + retour.nouveauRepas
                        + ' pour ' + retour.newNbPersonne + ' personne(s) ?';
                    var r = confirm(question);
                    if (r) {
                        $.ajax({
                            type: 'get',
                            url: '../ctrl/planningRepas.php?moment=' + 'ajout' + '&id=' + retour.idNouveauRepas + '&date=' + retour.date + '&nb=' + retour.newNbPersonne
                        }).done(function (data) {
                            if (data.ok) {
                                $('#succes').show();
                                $('#erreur').hide();
                            }
                            else {
                                $('#erreur ul').empty();
                                var ul = $('<ul />');
                                var li = $('<li />');
                                for (var j in data.erreur) {
                                    ul.append($('<li>').html(data.erreur[j]));
                                }
                                $('#succes').hide();

                                $('#erreur').append(ul).show();
                            }
                        }).fail(erreurCritique);
                        chargerRepasJour($('.date').val());
                        chargerRepasSansDate();
                    }

                }
            }
        }).fail(erreurCritique);
        chargerRepasJour($('.date').val());
        return false;  //a garder sinon va recharger la page
    });

});
