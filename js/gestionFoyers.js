function searchPersonne(search) {
    var userList = $('#user-list').empty();

    $.ajax({
        type: 'get',
        url: '../ctrl/gestionFoyer.php?action=search&search=' + search
    }).done(function (data) {
        for (var i in data.personnes) {
            var retour = jQuery.parseJSON(data.personnes[i]);
            userList.append('<li class="user">' + retour.nom + ' ' + retour.prenom + ' (' + retour.mail + ') <button type="button" value="' + retour.id + '" class="btn btn-sm btn-success btn-add-membre"> Ajouter personne </button>');
        }
        if (data.envoieMail) {
            $('.envoyer-mail').show();
        }
        else {
            $('.envoyer-mail').hide();
        }
        $('.btn-add-membre').click(function () {
            var idPersonne = $(this).val();

            $.ajax({
                type: 'get',
                url: '../ctrl/gestionFoyer.php?action=addMembreFoyer&id=' + idPersonne
            }).done(function (data) {
                if (data.ok) {
                    location.reload();
                }
            }).fail(erreurCritique);
        })
    }).fail(erreurCritique);
}

function searchFoyer(search) {
    var foyerList = $('#foyer-list').empty();

    $.ajax({
        type: 'get',
        url: '../ctrl/gestionFoyer.php?action=searchFoyer&search=' + search
    }).done(function (data) {
        for (var i in data.foyers) {
            var retour = jQuery.parseJSON(data.foyers[i]);
            foyerList.append('<li class="user">' + retour.nom + ' <button type="button" value="' + retour.id + '" class="btn btn-sm btn-success btn-dem-foyer"> Rejoindre le foyer </button>');
        }
        $('.btn-dem-foyer').click(function () {
            var idFoyer = $(this).val();

            $.ajax({
                type: 'get',
                url: '../ctrl/gestionFoyer.php?action=demFoyer&id=' + idFoyer
            }).done(function (data) {
                if (data.ok) {
                    location.reload();
                }
            }).fail(erreurCritique);
        })
    }).fail(erreurCritique);
}

var profilOuvert = false;

$(document).ready(function () {
    $.ajax({
        type: 'get',
        url: '../ctrl/estConnecte.php'
    }).done(function (data) {
        if (data.ok == true) {
            $(".connecte").show();
            $("#navigationBarre").show();
            $("#racourcisIndex").show();
        }
        else {
            location.href = '../index.php';
        }
    }).fail(function () {
        console.log(erreurCritique);
    });

    $('#btnAddFoyer').click(function () {
        var formData = new FormData();

        var nom = $('input[name=nom]').val();
        formData.append("nom", nom);

        $('.nomExiste').slideUp();
        $.ajax({
            type: 'post',
            url: '../ctrl/gestionFoyer.php?action=addFoyer',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data.ok) {
                location.reload();
            }
            else {
                $('.nomExiste').slideDown();
            }
        }).fail(erreurCritique);
    })

    $('.btn-moderer').click(function () {
        var idFoyer = $(this).val();

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=btnModererFoyer&id=' + idFoyer
        }).done(function (data) {
            if (data.ok) {
                $('.foyer-selectionne').slideDown();
                $('.nom-foyer').text(data.nomFoyer);
                $('.btn-suppr-foyer').val(idFoyer);
                $('#btn-modif-moderateur').val(idFoyer);
                $('#tempsSuppr').val(data.tempsSuppr);

                $('#nomFoyer').val(data.nomFoyer);

                $('#liste-membres').empty();
                for (var i in data.membres) {
                    var retour = jQuery.parseJSON(data.membres[i]);
                    $('#liste-membres').append('<option value="' + retour.id + '">' + retour.prenom + ' ' + retour.nom + '</option>');

                }
                if (data.demandes.length == 0) {
                    $('#demandes-list').html('Vous n\'avez reçu aucune demande.');
                }
                else {
                    for (var i in data.demandes) {
                        var retour = jQuery.parseJSON(data.demandes[i]);
                        $('#demandes-list').append(retour.prenom + ' ' + retour.nom +
                            ' <button type="button" class="btn btn-success accepter-demande" value="' + retour.id + '">Accepter</button>' +
                            ' <button type="button" class="btn btn-danger refuser-demande" value="' + retour.id + '">Refuser</button>');
                    }
                }
                $('#btn-demandes').html('<a>Gérer les demandes ('+data.demandes.length+')</a>');

                $('.accepter-demande').click(function () {
                    var idPersonne = $(this).val();
                    $.ajax({
                        type: 'get',
                        url: '../ctrl/gestionFoyer.php?action=accepterDemande&id=' + idPersonne
                    }).done(function (data) {
                        if (data.ok) {
                            location.reload();
                        }
                    }).fail(erreurCritique);
                })

                $('.refuser-demande').click(function () {
                    var idPersonne = $(this).val();
                    $.ajax({
                        type: 'get',
                        url: '../ctrl/gestionFoyer.php?action=refuserDemande&id=' + idPersonne
                    }).done(function (data) {
                        if (data.ok) {
                            location.reload();
                        }
                    }).fail(erreurCritique);
                })
            }
            else {
            }
        }).fail(erreurCritique);
    })

    $('.btn-quitter').click(function () {
        var idFoyer = $(this).val();
        $('.erreurQuitter').slideUp();

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=supprMembreFoyer&id=' + idFoyer
        }).done(function (data) {
            if (data.ok) {
                location.reload();
            }
            else {
                $('.erreurQuitter').slideDown();
            }
        }).fail(erreurCritique);
    })

    $('.btn-suppr-foyer').click(function () {
        var idFoyer = $(this).val();
        $('.suppr-foyer-impossible').slideUp();

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=supprFoyer&id=' + idFoyer
        }).done(function (data) {
            if (data.ok) {
                location.reload();
            }
            else {
                $('.suppr-foyer-impossible').slideDown();
            }
        }).fail(erreurCritique);
    })

    $('#btn-modif-nom').click(function () {
        var nom = $('#nomFoyer').val();
        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=modifNom&nom=' + nom
        }).done(function (data) {
            if (data.ok) {
                location.reload();
            }
            else {
                $('.nomExiste').slideDown();
            }
        }).fail(erreurCritique);
    })

    $('#btn-modif-moderateur').click(function () {
        var idPersonne = $('#liste-membres').val();
        var idFoyer = $(this).val();

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=modifModerateur&idPersonne=' + idPersonne + '&idFoyer=' + idFoyer
        }).done(function (data) {
            if (data.ok) {
                location.reload();
            }
        }).fail(erreurCritique);
    })

    $('#btn-modif-temps').click(function () {
        var tempsSuppr = $('#tempsSuppr').val();
        $('.modif-temps-impossible').slideUp();
        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=modifTempsSuppr&tempsSuppr=' + tempsSuppr
        }).done(function (data) {
            if (data.ok) {
                location.reload();
            }
            else {
                $('.modif-temps-impossible').slideDown();
            }
        }).fail(erreurCritique);
    })

    $('.btn-suppr-membre').click(function () {
        var idPersonne = $(this).val();
        var idFoyer = $(this).attr('data-foyer');

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=supprMembreFoyer&id=' + idFoyer + '&personne=' + idPersonne
        }).done(function (data) {
            if (data.ok) {
                location.reload();
            }
        }).fail(erreurCritique);
    })

    $('#btn-add-personne').click(function () {
        $('#add-personne').slideDown();
        $('#demandes').slideUp();
        $('#btn-demandes').toggleClass('active', false);
        $(this).addClass('active');
    })

    $('#btn-demandes').click(function () {
        $('#add-personne').slideUp();
        $('#demandes').slideDown();
        $('#btn-add-personne').toggleClass('active', false);
        $(this).toggleClass('active', true);
    })

    if ($('#search').val().length >= 3) {
        searchPersonne($('#search').val().toLowerCase());
    }

    $('#search').keypress(function () {
        var value = $('#search').val().toLowerCase();
        if (value.length >= 3) {
            searchPersonne(value);
        }
    });

    $('#searchFoyer').keypress(function () {
        var value = $('#searchFoyer').val().toLowerCase();
        if (value.length >= 3) {
            searchFoyer(value);
        }
    });

    $('.foyer-default').click(function () {
        var idFoyer = $(this).val();

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=setDefault&id=' + idFoyer
        }).done(function (data) {

        }).fail(erreurCritique);
    })

    $('#btn-envoie-mail').click(function () {
        $.ajax({
            type: 'get',
            url: '../ctrl/gestionFoyer.php?action=envoieMail&mail=' + $('#search').val()
        }).done(function (data) {

        }).fail(erreurCritique);
    })

});