$(document).ready(function () {
    $("#btnInscription").click(function () {
        var formData = new FormData();

        var nom = $('input[name=nom]').val();
        formData.append("nom", nom);

        var prenom = $('input[name=prenom]').val();
        formData.append("prenom", prenom);

        var mail = $('input[name=mail]').val();
        formData.append("mail", mail);

        var mdp = $('input[name=mdp]').val();
        formData.append("mdp", mdp);

        var mdpConfirm = $('input[name=mdpConfirm]').val();
        formData.append("mdpConfirm", mdpConfirm);

        $('.mailFormat').slideUp();
        $('.mailExiste').slideUp();
        $('.nom').slideUp();
        $('.prenom').slideUp();
        $('.mdpIncorrect').slideUp();
        $('.mdpIdentique').slideUp();
        $.ajax({
            type: 'post',
            url: '../ctrl/inscription.php',
            data : formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if(data.ok) {
                location.href = '../index.php';
            }
            else{
                if(!data.mailValide){
                    $('.mailFormat').slideDown();
                }
                if(!data.mailExiste){
                    $('.mailExiste').slideDown();
                }
                if(!data.nomValide){
                    $('.nom').slideDown();
                }
                if(!data.prenomValide){
                    $('.prenom').slideDown();
                }
                if(!data.mdpIdentique){
                    $('.mdpIdentique').slideDown();
                }
            }

        }).fail(erreurCritique);
        return false;  //a garder sinon va recharger la page
    });
});