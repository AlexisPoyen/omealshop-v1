function erreurCritique(){
    $('body').html(
        'Une erreur irrécupérable est survenue. <br />'
        +'Merci de contcter l\'admin <br/>'
        +(true ? 'co' : 'e@e.com')+'ntact' + '@' + 'mons' + 'ite.com'
    );
}

var profilOuvert = false;

$(document).ready(function () {
    $.ajax({
        type: 'get',
        url: '../ctrl/estConnecte.php'
    }).done(function (data) {
        if(data.ok == true){
            $(".connecte").show();
            $("#navigationBarre").show();
            $("#racourcisIndex").show();
            var url = document.location.href;
            var NomDuFichier = url.substring(url.lastIndexOf("/") + 1);
            if (data.nomFoyer == 'aucun' && NomDuFichier.indexOf('gestionFoyer') == -1) {
                location.href = '../view/gestionFoyer.php?erreurFoyer=true';
            }
        }
        else{
            $("#connexion").show();
            $("#introNonConnecte").show();
        }
    }).fail(function () {
        console.log(erreurCritique);
    });

    $("#connexion").submit(function () {
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data : $(this).serialize()
        }).done(function (data) {
            if(data.ok) {
                location.href = "../index.php";
            }
            else{
                console.log('erreur');
            }

        }).fail(erreurCritique);
        return false;  //a garder sinon va recharger la page
    });

    var profilOuvert = false;
     $("#profil").click(function () {
         if(profilOuvert){
             $("#monProfil").slideUp();
             profilOuvert = false;
         }
         else{
             $("#monProfil").slideDown();
             profilOuvert = true;
         }

     });

    var foyerOuvert = false;
    $("#foyer").click(function () {
        if(profilOuvert){
            $("#foyers").slideUp();
            profilOuvert = false;
        }
        else{
            $("#foyers").slideDown();
            profilOuvert = true;
        }

    });

    $("#deconnexion").click(function () {
        $.ajax({
            type: 'get',
            url: '../ctrl/deconnexion.php'
        }).done(function (data) {
            if(data.ok) {
                location.href = '../index.php';
            }
        }).fail(erreurCritique);
    })

    $('.ln-foyer').click(function () {
        var idFoyer = $(this).val();

        $.ajax({
            type: $(this).attr('method'),
            url: '../ctrl/gestionFoyer.php?action=switchFoyer&id='+idFoyer
        }).done(function (data) {
            if(data.ok) {
                location.href = '../index.php';
            }
        }).fail(erreurCritique);

    })

    $('#inscription').click(function () {
        document.location.href = '../view/inscription.php';
    })

    $('#passwordForgotten').click(function () {
        location.href = '../view/motDePasseOublie.php';
    })
});