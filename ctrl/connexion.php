<?php
session_start();
require_once '../model/personne.php';
require_once '../model/session.php';
require_once '../model/repas_plannifie.php';

//////////////// CALCUL DE TOUTES LES VARIABLES
$drapeau = false;
$message = 'Erreur';
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $drapeau = false;
    $personne = personne::getByMail($username);


    if (isset($personne)) {
        $retour = $personne->connecter($password);
        if ($retour == 0) {
            $drapeau = true;
            $message = 'Bonjour Mr  ' . $username;
        }
        elseif($retour == -1)
            $message = "Votre compte n'a pas été activé";
        elseif ($retour == -2)
            $message = "Identifiants inconnus";
    }
    else
        $message = "Identifiants inconnus";

    $foyerDefault = membre_foyer::getFoyerParDefaut($personne->getId());
    if($foyerDefault != null){
        repas_plannifie::supprAncienRepas($foyerDefault);
        $session = unserialize($_SESSION['session']);
        $session->setFoyer($foyerDefault);
        $_SESSION['session'] = serialize($session);
    }
}

$obj = new stdClass();
$obj->ok = $drapeau;
$obj->message = $message;

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
// on ne met pas la fin du php pour pas qu'il envoie les headers entre deux