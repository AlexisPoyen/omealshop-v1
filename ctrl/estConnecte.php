<?php
session_start();
require_once '../model/session.php';
require_once '../model/foyer.php';
require_once '../model/personne.php';
$retour = false;
$obj = new stdClass();
if (isset($_SESSION['session'])) {
    $session = unserialize($_SESSION['session']);
    $personne = $session->getPersonne();
    $foyer = $session->getFoyer();
    if($foyer != null) {
        $obj->nomFoyer = $foyer->getNom();
        $obj->idFoyer = $foyer->getId();
    }
    else
        $obj->nomFoyer = 'aucun';
    $retour = true;
}

$obj->ok = $retour;

//////////////// CALCUL DE TOUTES LES VARIABLES

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
// on ne met pas la fin du php pour pas qu'il envoie les headers entre deux