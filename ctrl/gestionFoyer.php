<?php
session_start();
require_once '../model/personne.php';
require_once '../model/foyer.php';
require_once '../model/session.php';

$obj = new stdClass();
$action = $_GET['action'];
$retour = false;

if (isset($_SESSION['session'])) {
    $session = unserialize($_SESSION['session']);
    $personne = $session->getPersonne();

    if ($action == 'addFoyer') {
        $nomFoyer = $_POST['nom'];
        if (!foyer::existeDeja($nomFoyer)) {
            $retour = foyer::addFoyer($nomFoyer, $personne);
            if($retour && membre_foyer::getFoyerParDefaut($personne->getId()) == null){
                membre_foyer::setDefault(foyer::getByNom($nomFoyer), $personne);
            }
        } else {
            $retour = false;
            $erreur = 'Le nom du foyer est déjà utilisé';
        }
    }
    else if ($action == 'supprMembreFoyer' && isset($_GET['id'])) {
        $idFoyer = $_GET['id'];
        if (isset($_GET['personne']))
            $retour = membre_foyer::supprMembre($idFoyer, $_GET['personne']);
        else
            $retour = membre_foyer::supprMembre($idFoyer, $personne->getId());
    }
    else if ($action == 'supprFoyer' && isset($_GET['id'])) {
        $foyer = foyer::getById($_GET['id']);
        $retour = $foyer->suppression();
    }
    else if ($action == 'btnModererFoyer' && isset($_GET['id'])) {
        $foyer = foyer::getById($_GET['id']);
        $obj->nomFoyer = $foyer->getNom();
        $membres = membre_foyer::getMembresFoyer($foyer->getId());
        $obj->membres = Array();
        if (sizeof($membres) > 0)
            foreach ($membres as $membre) {
                if ($membre->getId() != $personne->getId())
                    array_push($obj->membres, '{"id": ' . $membre->getId() . ', "nom": "' . $membre->getNom() . '", "prenom": "' . $membre->getPrenom() . '"}');
            }
        $demandes = membre_foyer::getDemandesMembres($foyer->getId());
        $obj->demandes = Array();
        if (sizeof($demandes) > 0) {
            foreach ($demandes as $demande) {
                array_push($obj->demandes, '{"id": ' . $demande->getId() . ', "nom": "' . $demande->getNom() . '", "prenom": "' . $demande->getPrenom() . '"}');

            }
        }
        $obj->tempsSuppr = $foyer->getTempsSuppr();
        $_SESSION['foyer_moderation_id'] = $_GET['id'];
        $retour = true;

    }
    else if ($action == 'modifNom' && isset($_GET['nom'])) {
        $nom = $_GET['nom'];
        if (foyer::existeDeja($nom)) {
            $retour = false;
            $erreur = 'Le nom du foyer est déjà utilisé';
        } else {
            $foyer = foyer::getById($_SESSION['foyer_moderation_id']);
            $retour = $foyer->setNom($nom);
        }
    }

    else if ($action == 'modifModerateur' && isset($_GET['idPersonne']) && isset($_GET['idFoyer'])) {
        $foyer = foyer::getById($_GET['idFoyer']);
        $personne = personne::getById($_GET['idPersonne']);
        $retour = $foyer->changeModerateur($personne);

    }
    else if ($action == 'search' && isset($_GET['search'])) {
        $search = $_GET['search'];
        $personnes = personne::getPersonnesLikeNotMembre($search, $_SESSION['foyer_moderation_id']);
        $obj->personnes = Array();

        if (sizeof($personnes) > 0)
            foreach ($personnes as $personne) {
                array_push($obj->personnes, '{"nom": "' . $personne->getNom() . '", "prenom": "' . $personne->getPrenom() .
                    '", "id": ' . $personne->getId() . ', "mail": "' . $personne->getMail() . '" }');
            }

        if (sizeof($personnes) == 0 && filter_var($search, FILTER_VALIDATE_EMAIL))
            $obj->envoieMail = true;
        else
            $obj->envoieMail = false;
        $retour = true;
    }
    else if ($action == 'addMembreFoyer' && isset($_GET['id'])) {
        $personne = personne::getById($_GET['id']);
        $retour = membre_foyer::addMembre(foyer::getById($_SESSION['foyer_moderation_id']), $personne);

        if (($foyer = membre_foyer::getFoyerParDefaut($personne->getId())) == null && $retour) {
            $retour = membre_foyer::setDefault(foyer::getById($_SESSION['foyer_moderation_id']), $personne);
        }
    }
    else if ($action == 'searchFoyer' && isset($_GET['search'])) {
        $search = $_GET['search'];
        $foyers = foyer::getFoyersLike($search);
        $obj->foyers = Array();

        if (sizeof($foyers) > 0)
            foreach ($foyers as $foyer) {
                array_push($obj->foyers, '{"nom": "' . $foyer->getNom() . '", "id": ' . $foyer->getId() . '}');
            }
        $retour = true;
    }
    else if ($action == 'demFoyer' && isset($_GET['id'])) {
        $retour = membre_foyer::addCandidat(foyer::getById($_GET['id']), $personne);
    }
    else if ($action == 'accepterDemande' && isset($_GET['id'])) {
        $membre = membre_foyer::getById($_GET['id'], $_SESSION['foyer_moderation_id']);
        if ($membre != false)
            $retour = $membre->setEtat(1);
        else
            $retour = false;
        if (($foyer = membre_foyer::getFoyerParDefaut($_GET['id'])) == null && $retour) {
            $retour = membre_foyer::setDefault($membre->getFoyer(), personne::getById($_GET['id']));
        }
    }
    else if ($action == 'refuserDemande' && isset($_GET['id'])) {
        $retour = membre_foyer::supprMembre($_SESSION['foyer_moderation_id'], $_GET['id']);
    }
    else if ($action == 'switchFoyer' && isset($_GET['id'])) {
        $foyer = foyer::getById($_GET['id']);
        $session->setFoyer($foyer);
        $_SESSION['session'] = serialize($session);
        $retour = true;
    }
    else if ($action == 'setDefault' && isset($_GET['id'])) {
        $foyer = foyer::getById($_GET['id']);
        $retour = membre_foyer::setDefault($foyer, $personne);
        $session->setFoyer($foyer);
        $_SESSION['session'] = serialize($session);
    }
    else if ($action == 'modifTempsSuppr' && isset($_GET['tempsSuppr'])) {
        $foyer = foyer::getById($_SESSION['foyer_moderation_id']);
        $tempsSuppr = $_GET['tempsSuppr'];
        if ($tempsSuppr >= 1 && $tempsSuppr <= 8)
            $retour = $foyer->setTempsSuppr($tempsSuppr);
        else
            $retour = false;
    }
    else if ($action == 'getTempsSuppr') {
        $foyer = $session->getFoyer();
        if ($foyer != null) {
            $obj->tempsSuppr = $foyer->getTempsSuppr();
            $retour = true;
        } else {
            $retour = false;
        }
    }
    else if($action == 'envoieMail' && isset($_GET['mail'])){
        $destinataire = $_GET['mail'];
        $sujet = "Rejoignez OMealShops";
        $entete = "From: no_reply@omealshop.com \n";
        $message = 'Bonjour,

Un utilisateur de OMealShop souhaite vous ajouter à son foyer. 

Veuillez vous rendre sur le site pour vous inscrire, puis demandez à rejoindre le foyer : '.foyer::getById($_SESSION['foyer_moderation_id'])->getNom().'

' . TXT_addresse_site . '/view/home.php
            
---------------
Ceci est un mail automatique, Merci de ne pas y répondre.';

        $retour = mail($destinataire, $sujet, $message, $entete); // Envoi du mail
    }

}

$obj->ok = $retour;
$obj->erreur = $erreur;

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
// on ne met pas la fin du php pour pas qu'il envoie les headers entre deux
