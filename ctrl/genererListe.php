<?php
session_start();
require_once '../model/DB.php';
require_once '../model/personne.php';
require_once '../model/categorie_ingredients.php';
require_once '../model/categorie_produits.php';
require_once '../model/categorie_repas.php';
require_once '../model/ingredients.php';
require_once '../model/ingredients_listes.php';
require_once '../model/ingredients_repas.php';
require_once '../model/produit.php';
require_once '../model/produits_listes.php';
require_once '../model/repas_plannifie.php';
require_once '../model/repas.php';

$session = unserialize($_SESSION['session']);
$personne = $session->getPersonne();
$foyer = $session->getFoyer();

$drapeau = false;
$obj = new stdClass();

if (isset($personne) && isset($_GET['dateDeb']) && isset($_GET['dateFin'])) {
    $dateDeb = $_GET['dateDeb'];
    $dateFin = $_GET['dateFin'];

    $obj->ingredientsRepas = Array();
    $repasPlannifiés = repas_plannifie::getRepasBetweenDate($dateDeb, $dateFin, $foyer);
    $repasNonPlannifiés = repas_plannifie::getRepasBetweenDate('2000-01-01 00:00:00', '2000-01-01 00:05:00', $foyer);
    if (sizeof($repasNonPlannifiés) > 0)
        foreach ($repasNonPlannifiés as $repasNonPlannifié) {
            $ingredientsRepas = $repasNonPlannifié->getIngredients();
            foreach ($ingredientsRepas as $ingredientRepas) {
                array_push($obj->ingredientsRepas, '{"idIngredient": ' . $ingredientRepas->getIngredients()->getId() . ', "nomIngredient": "' . $ingredientRepas->getIngredients()->getNom()
                    . '", "categorieIngredient": "' . $ingredientRepas->getIngredients()->getCategorieIngredients()->getIntitule()
                    . '", "quantite": ' . $ingredientRepas->getQuantite() . ', "unite": "' . $ingredientRepas->getIngredients()->getUnite() . '"}');
            }

        }

    if (sizeof($repasPlannifiés) > 0)
        foreach ($repasPlannifiés as $repasPlannifié) {
            $ingredientsRepas = $repasPlannifié->getIngredients();
            foreach ($ingredientsRepas as $ingredientRepas) {
                array_push($obj->ingredientsRepas, '{"idIngredient": ' . $ingredientRepas->getIngredients()->getId() . ', "nomIngredient": "' . $ingredientRepas->getIngredients()->getNom()
                    . '", "categorieIngredient": "' . $ingredientRepas->getIngredients()->getCategorieIngredients()->getIntitule()
                    . '", "quantite": ' . $ingredientRepas->getQuantite() . ', "unite": "' . $ingredientRepas->getIngredients()->getUnite() . '"}');
            }

        }

    $obj->ingredients = Array();
    $ingredientsListe = ingredients_listes::getByFoyer($foyer);
    if (sizeof($ingredientsListe) > 0)
        foreach ($ingredientsListe as $ingredient) {
            array_push($obj->ingredients, '{"idIngredientListe": ' . $ingredient->getIngredients()->getId() . ', "nomIngredient": "' . $ingredient->getIngredients()->getNom()
                . '", "categorieIngredient": "' . $ingredient->getIngredients()->getCategorieIngredients()->getIntitule()
                . '", "quantite": ' . $ingredient->getQuantite() . ', "unite": "' . $ingredient->getIngredients()->getUnite() . '", "commentaire": "'.$ingredient->getCommentaire().'"}');
        }

    $obj->produits = Array();
    $produitsListe = produits_listes::getByFoyer($foyer);
    if (sizeof($produitsListe) > 0)
        foreach ($produitsListe as $produit) {
            array_push($obj->produits, '{"idProduitListe": ' . $produit->getProduit()->getId() . ', "nomProduit": "' . $produit->getProduit()->getNom()
                . '", "categorieProduit": "' . $produit->getProduit()->getCategorieProduit()->getIntitule()
                . '", "quantite": ' . $produit->getQuantite() . ', "unite": "' . $produit->getProduit()->getUnite() . '", "commentaire": "'.$produit->getCommentaire().'"}');
        }

        if(sizeof($repasNonPlannifiés) > 0)
    foreach ($repasNonPlannifiés as $repasNonPlannifié) {
        $repasNonPlannifié->supprimer();
    }
    $drapeau = true;
}


$obj->ok = $drapeau;

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);