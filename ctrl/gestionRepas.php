<?php
/**
 * Created by PhpStorm.
 * User: punk
 * Date: 31/07/17
 * Time: 14:01
 */

session_start();
require_once '../model/DB.php';
require_once '../model/repas.php';
require_once '../model/ingredients.php';
require_once '../model/ingredients_repas.php';
require_once '../model/session.php';
require_once '../model/foyer.php';
$session = unserialize($_SESSION['session']);
$foyer = $session->getFoyer();
$action = $_GET['action'];
$obj = new stdClass();

if (isset($action)) {
    if ($action == 'getRepas' && isset($_GET['cat'])) {
        $categorie = $_GET['cat'];
        $repas = repas::getRepasByCat($categorie);
        $obj->listeRepas = Array();
        foreach ($repas as $repa) {
            array_push($obj->listeRepas, '{"idRepas": ' . $repa->getId() . ', "nomRepas": "' . $repa->getNom() . '", "typeRepas": "' . $repa->getTypeRepas() .
                '", "categorieRepas": "' . $repa->getCategorieRepas()->getIntitule() . '", "createur": ' . (($repa->getCreateur() != null) ? $repa->getCreateur() : 'null') . '}');
        }
        $retour = true;
    }

    if ($action == 'getIngredients' && isset($_GET['id'])) {
        $repas = repas::getById($_GET['id']);
        $obj->idCategorie = $repas->getCategorieRepas()->getId();
        $obj->nomRepas = $repas->getNom();
        $obj->type = $repas->getTypeRepas();
        $obj->supprimable = ($repas->getCreateur() != null ? true : false);
        $ingredients = $repas->getIngredients();
        $obj->ingredients = Array();
        if (sizeof($ingredients) > 0)
            foreach ($ingredients as $ingredient) {
                array_push($obj->ingredients, '{"idIngredient": ' . $ingredient->getIngredients()->getId() . ', "idCategorie": ' . $ingredient->getIngredients()->getCategorieIngredients()->getId() .
                    ', "unite": "' . $ingredient->getIngredients()->getUnite() . '", "quantite": ' . $ingredient->getQuantite() . '}');
            }
        $retour = true;
    }

    if ($action == 'getCategories') {
        $categories = categorie_ingredients::getListe();
        $obj->categories = Array();
        foreach ($categories as $category) {
            array_push($obj->categories, '{"idCat": ' . $category->getId() . ', "nomCat": "' . $category->getIntitule() . '"}');
        }
        $retour = true;
    }

    if ($action == 'getIngredient' && isset($_GET['id'])) {
        $ingredients = ingredients::getListeByCat($_GET['id']);
        $obj->ingredients = Array();
        foreach ($ingredients as $ingredient) {
            array_push($obj->ingredients, '{"idIng": ' . $ingredient->getId() . ', "nomIng": "' . $ingredient->getNom() . '"}');
        }
        $retour = true;
    }

    if ($action == 'getUnite' && isset($_GET['id'])) {
        $ingredient = ingredients::getById($_GET['id']);
        $obj->unite = $ingredient->getUnite();
        $retour = true;
    }

    if ($action == 'editRepas') {
        $drapeau = true;
        $nomRepas = $_POST['nom-repas'];
        $categorieRepas = $_POST['lst-categorie-repas'];
        $typeRepas = $_POST['type-repas'];
        $ingredientsForm = $_POST['ingredient'];
        $quantitesForm = $_POST['quantite'];
        if ($_POST['idRepas'] == 'new') {
            if ($nomRepas == '' || $categorieRepas == '' || $typeRepas == '') {
                $drapeau = false;
                $erreur = 3;
            }
            if ($drapeau) {
                if (repas::existeDeja($nomRepas, $foyer->getId())) {
                    $erreur = 1;
                    $drapeau = false;
                }
            }

            if ($drapeau) {
                $drapeau = repas::addRepas($nomRepas, $foyer->getId(), $categorieRepas, $typeRepas);
                $repas = repas::getByNomFoyer($nomRepas, $foyer->getId());

                $i = 0;
                if (isset($_POST['ingredient'])) {
                    foreach ($ingredientsForm as $ingredient) {
                        $quantite = $quantitesForm[$i];
                        $flag = ingredients_repas::addIngredient($repas->getId(), $ingredient, $quantite);
                        ++$i;
                        if (!$flag) {
                            $drapeau = false;
                            $erreur = 2;
                            break;
                        }
                    }
                }
            }
            $retour = $drapeau;
        } else {
            $repas = repas::getById($_POST['idRepas']);
            $ingredients = $repas->getIngredients();

            if ($repas->getCreateur() != null) {
                $repas->setCategorieRepas(categorie_repas::getById($categorieRepas));
                $repas->setNom($nomRepas);
                $repas->setTypeRepas($typeRepas);
                $i = 0;
                if (isset($_POST['ingredient'])) {
                    foreach ($ingredientsForm as $ingForm) {
                        $flag = false;
                        $quantite = $quantitesForm[$i];
                        if (isset($ingredients)) {
                            foreach ($ingredients as $ingredient) {
                                if ($ingForm == $ingredient->getIngredients()->getId()) {
                                    $ingredient->setQuantite($quantite);
                                    $flag = true;
                                }
                            }
                        }
                        if (!$flag) {
                            $flag = ingredients_repas::addIngredient($repas->getId(), $ingForm, $quantite);
                        }
                        if (!$flag) {
                            $drapeau = false;
                            $erreur = 2;
                            break;
                        }
                        ++$i;
                    }
                }

                if (isset($ingredients)) {
                    foreach ($ingredients as $ingredient) {
                        $flag = false;
                        if (isset($_POST['ingredient'])) {
                            foreach ($ingredientsForm as $ingForm) {
                                if ($ingredient->getIngredients()->getId() == $ingForm)
                                    $flag = true;
                            }
                            if (!$flag) {
                                $ingredient->remove();
                            }
                        }
                        else{
                            $ingredient->remove();
                        }
                    }
                }
            } else {
                $repasFoyer = repas::getByNomFoyer($nomRepas, $foyer->getId());

                if($repasFoyer == false) {
                    $drapeau = repas::addRepas($nomRepas, $foyer->getId(), $categorieRepas, $typeRepas);
                    $repas = repas::getByNomFoyer($nomRepas, $foyer->getId());
                }

                $repas->setCategorieRepas(categorie_repas::getById($categorieRepas));
                $repas->setNom($nomRepas);
                $repas->setTypeRepas($typeRepas);

                $i = 0;
                if (isset($_POST['ingredient'])) {
                    foreach ($ingredientsForm as $ingredient) {
                        $quantite = $quantitesForm[$i];
                        $flag = ingredients_repas::addIngredient($repas->getId(), $ingredient, $quantite);
                        ++$i;
                        if (!$flag) {
                            $drapeau = false;
                            $erreur = 2;
                            break;
                        }
                    }
                }
            }
        }
        $retour = $drapeau;
    }

    if ($action == 'supprRepas' && isset($_GET['id'])) {
        $repas = repas::getById($_GET['id']);
        if ($repas->getCreateur() == $foyer->getId()) {
            $retour = $repas->suppr();
        }
    }
} else $retour = false;


$obj->ok = $retour;
$obj->erreur = $erreur;

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
?>