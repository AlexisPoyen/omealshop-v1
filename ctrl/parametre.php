<?php
require_once '../model/session.php';
require_once '../model/personne.php';

if (isset($_GET['action']) && isset($_SESSION['session'])) {
    $session = unserialize($_SESSION['session']);
    $personne = $session->getPersonne();
    $action = $_GET['action'];

    if ($action == 'modifInfos') {
        $mail = $_POST['mail'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $retour = true;

        if (strlen($mail) == 0 || !filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $erreur = 2;
            $retour = false;
        }
        if ($retour) {
            if (personne::getByMail($mail) != false && $personne->getMail() != $mail) {
                $erreur = 1;
                $retour = false;
            } else {
                $retour = $personne->setMail($mail);
            }
        }

        if ($retour)
            $retour = $personne->setNom($nom);
        if ($retour)
            $retour = $personne->setPrenom($prenom);

        $session->setPersonne($personne);
        $_SESSION['session'] = serialize($session);
    }

    if ($action == 'modifFoyer' && isset($_GET['id'])) {
        $retour = membre_foyer::setDefault(foyer::getById($_GET['id']), $personne);
        $session->setFoyer(foyer::getById($_GET['id']));
        $_SESSION['session'] = serialize($session);
    }

    if ($action == 'modifMDP' && isset($_POST['formerPWD']) && isset($_POST['pwd']) && isset($_POST['confirmPWD'])) {
        $formerPWD = $_POST['formerPWD'];
        $pwd = $_POST['pwd'];
        $confirmPWD = $_POST['confirmPWD'];
        $drapeau = $personne->connecter($formerPWD);

        if ($drapeau == 0) {
            if ($pwd == $confirmPWD) {
                if (strlen($mdp) > 8 || preg_match('#[A-Z]#', $mdp) == 1 || preg_match('#[0-9]#', $mdp) ==1 || preg_match('/[^a-zA-Z0-9]+/', $mdp) == 1) {
                    $drapeau = $personne->changerMotDePasse($pwd);
                    if ($drapeau) {
                        $retour = 0;
                        $session->setPersonne($personne);
                        $_SESSION['session'] = serialize($session);
                    }
                } else $retour = 3;
            } else
                $retour = 2;
        } else
            $retour = 1;
    }
    if ($action == 'supprimerCompte') {
        $drapeau = true;
        $foyers = membre_foyer::getFoyersPersonne($personne->getId());
        if (sizeof($foyers))
            foreach ($foyers as $foyer) {
                if ($foyer->getPersonneCreateur()->getId() == $personne->getId()) {
                    $erreur = 1;
                    $drapeau = false;
                    break;
                }
            }
        if ($drapeau) {
            $retour = $personne->suppr();
        }
    }
} else
    $retour = false;

$obj = new stdClass();
$obj->ok = $retour;
$obj->erreur = $erreur;

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
?>