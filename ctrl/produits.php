<?php
session_start();
require_once '../model/DB.php';
require_once  '../model/session.php';
require_once  '../model/personne.php';
require_once  '../model/foyer.php';
require_once  '../model/ingredients.php';
require_once  '../model/produit.php';
require_once  '../model/produits_listes.php';
require_once  '../model/ingredients_listes.php';

$session = unserialize($_SESSION['session']);
$personne = $session->getPersonne();
$foyer = $session->getFoyer();
$action = $_GET['action'];

$retour = false;

if($action == 'getProduits' && isset($_GET['cat'])){
    $cat = $_GET['cat'];
    $produits = produit::getListeByCat($cat);
}

if($action == 'getIngredients' && isset($_GET['cat'])){
    $cat = $_GET['cat'];
    $produits = ingredients::getListeByCat($cat);
}

if($action == 'nonAlimentaire' && isset($_POST['produit']) && isset($_POST['quantite'])){
    $produit = produit::getById($_POST['produit']);
    $retour = produits_listes::addProduitToListe($produit, $foyer, $personne, $_POST['quantite'], $_POST['commentaire']);
}

if($action == 'alimentaire' && isset($_POST['ingredient']) && isset($_POST['quantite'])){
    $ingredient = ingredients::getById($_POST['ingredient']);
    $retour = ingredients_listes::addProduitToListe($ingredient, $foyer, $personne, $_POST['quantite'], $_POST['commentaire']);
}

if($action == 'getListeProduit'){
    $listeProduits = produits_listes::getByFoyer($foyer);
}

if($action == 'getListeIngredient'){
    $listeIngredients = ingredients_listes::getByFoyer($foyer);
}

if($action == "supprProduit" && isset($_GET['id'])){
    $produit = produits_listes::getById($_GET['id']);
    $retour = $produit->suppr();
}

if($action == "supprIngredient" && isset($_GET['id'])){
    $ingredient = ingredients_listes::getById($_GET['id']);
    $retour = $ingredient->suppr();
}

if($action == 'supprIngredient' && isset($_GET['idIngredient'])){
    $retour = ingredients_listes::supprByIdIngredient($_GET['idIngredient']);
}


if($action == 'supprProduit' && isset($_GET['idProduit'])){
    $retour = produits_listes::supprByIdProduit($_GET['idProduit']);
}

$obj = new stdClass();
$obj->ok = $retour;
$obj->idProduit = $idProduit;
$obj->produits = Array();
$obj->liste = Array();
if(count($produits) > 0)
    foreach ($produits as $prod) {
        array_push($obj->produits, '{"idProduit": '.$prod->getId().', "nomProduit": "'.$prod->getNom().'"}');
    }
if(count($listeProduits) > 0)
    foreach ($listeProduits as $prod) {
        array_push($obj->liste, '{"idProduit": '.$prod->getId().', "nomProduit": "'.$prod->getProduit()->getNom().
            '", "nomDemandeur": "'.$prod->getDemandeur()->getNom().'", "prenomDemandeur": "'.$prod->getDemandeur()->getPrenom().
            '", "quantite": '.$prod->getQuantite().', "catProduit": "'.$prod->getProduit()->getCategorieProduit()->getIntitule().
            '", "commentaire": "'.$prod->getCommentaire().'"}');
    }

if(count($listeIngredients) > 0)
    foreach ($listeIngredients as $prod) {
        array_push($obj->liste, '{"idProduit": '.$prod->getId().', "nomProduit": "'.$prod->getIngredients()->getNom().
            '", "nomDemandeur": "'.$prod->getDemandeur()->getNom().'", "prenomDemandeur": "'.$prod->getDemandeur()->getPrenom().
            '", "quantite": '.$prod->getQuantite().', "catProduit": "'.$prod->getIngredients()->getCategorieIngredients()->getIntitule().
            '", "commentaire": "'.$prod->getCommentaire().'"}');
    }


////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);